package com.webdealer.dashrider.ui.activities

import android.content.Intent
import android.os.Bundle
import com.webdealer.dashrider.R
import com.webdealer.dashrider.ui.fragments.PinVerification
import com.webdealer.dashrider.utils.AppStorage
import kotlinx.android.synthetic.main.activity_verification.*

class Verification : Base() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        //  adapter = SignUpPager(supportFragmentManager)
        back.setOnClickListener {
            onBackPressed()
        }
        setFragment(PinVerification.newInstance())
    }

    override fun onBackPressed() {
        AppStorage.clearSession()
        val login = Intent(this, Login::class.java)
        login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        startActivity(login)
        super.onBackPressed()
    }

}


