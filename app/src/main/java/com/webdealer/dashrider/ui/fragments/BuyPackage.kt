package com.webdealer.dashrider.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.models.RidePackage
import com.webdealer.dashrider.ui.activities.Billing
import com.webdealer.dashrider.ui.adapters.PackagesAdapter
import kotlinx.android.synthetic.main.fragment_package.*

class BuyPackage : BaseUI() {
    companion object {
        fun newInstance() = BuyPackage()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_package, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        val packages = mutableListOf<RidePackage>()
        packages.add(RidePackage())
        packages.add(RidePackage())
        packages.add(RidePackage())
        packages.add(RidePackage())
        packages.add(RidePackage())
        rv.setItemViewCacheSize(packages.size)
        rv.adapter = PackagesAdapter(packages)
        cont.setOnClickListener {
            startActivity(Intent(it.context, Billing::class.java))
        }
    }
}