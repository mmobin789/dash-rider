package com.webdealer.dashrider.models

class TimeSlot(val timeSlot: String, var isSet: Boolean) {


    fun getStartTime(): String {
        val lastIndex = timeSlot.length
        val startIndex = lastIndex - 2
        val meridian = timeSlot.substring(startIndex, lastIndex)
        return "${timeSlot.split(" ", "-", " ")[0]} $meridian"
    }

}