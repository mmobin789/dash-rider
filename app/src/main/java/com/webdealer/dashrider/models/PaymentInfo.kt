package com.webdealer.dashrider.models

import com.webdealer.dashrider.utils.AppStorage

data class PaymentInfo(val cardNumber: String, val cvv: String, val holderName: String, val expDate: String) {
    val email = AppStorage.getUser().email

    val isEdit = "0"
}