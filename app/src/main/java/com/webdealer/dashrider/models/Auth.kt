package com.webdealer.dashrider.models

import com.webdealer.dashrider.utils.AppStorage

data class Auth(val email: String, val password: String) {
    var name = ""
    var phone = ""
    var code = ""
    var rideID = "-1"
    val type = "2"
    var cardNumber = ""
    val fcmToken: String = AppStorage.getFCM()

    constructor(name: String, email: String, phone: String, password: String) : this(email, password) {
        this.name = name
        this.phone = phone
    }

    fun isValid() = name.isNotBlank() && phone.isNotBlank() && email.isNotBlank() && password.isNotBlank()
}