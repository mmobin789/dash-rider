package com.webdealer.dashrider.contracts

interface OnApiErrorListener {
    fun onApiError(error: String)
}