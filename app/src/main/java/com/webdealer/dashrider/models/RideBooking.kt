package com.webdealer.dashrider.models

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.webdealer.dashrider.utils.AppStorage

class RideBooking {
    var carType = "DashGo"
    var picklat = ""
    var picklng = ""
    var droplat = "0"
    var droplng = "0"
    var isRideNow = "1"
    @SerializedName("pickup_date")
    var pickupDate = (System.currentTimeMillis() / 1000L).toString()
    val email = AppStorage.getUser().email

    var localPickUpAddress: String? = null

    var localDropOffAddress: String? = "N/A"

    fun toJsonString(): String =
            Gson().toJson(this)

}