package com.webdealer.dashrider.ui.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import com.stripe.android.TokenCallback
import com.stripe.android.model.Token
import com.webdealer.dashrider.R
import com.webdealer.dashrider.api.Api
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.Card
import com.webdealer.dashrider.models.PaymentInfo
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.toolbar.*
import java.lang.Exception
import java.util.*

class Payment : Base(), OnApiListener, TokenCallback {
    var card = ""
    var cvv = ""
    var name = ""
    var exp = ""
    var expYear = 0
    var expMonth = 0

    override fun onSuccess(token: Token) {
        dashRiderImpl.addPaymentInfo(this, PaymentInfo(card, cvv, name, exp), this)
    }

    override fun onError(error: Exception) {
        error.printStackTrace()
        showToast(error.localizedMessage)
        dismissProgressBar()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        init()
    }

    private fun init() {
        save.visibility = View.VISIBLE
        setToolbarTitle("Add Credit Card")
        back.visibility = View.VISIBLE

        back.setOnClickListener {
            onBackPressed()
        }

        save.setOnClickListener {
            val valid = etCardNo.text.isNotBlank() && etCvv.text.isNotBlank() && etName.text.isNotBlank() && etExpDate.text.isNotBlank() && expYear > 0
            if (valid) {
                card = etCardNo.text.toString()
                cvv = etCvv.text.toString()
                name = etName.text.toString()
                exp = etExpDate.text.toString()
                showProgressBar()
                Api.stripe(this, com.stripe.android.model.Card(card, expMonth, expYear, cvv), this)
            } else
                showToast("Fill Remaining")
        }

        etExpDate.setOnClickListener {
            chooseDOB(it)
        }
        parseCardData()
    }

    private fun parseCardData() {
        val card = intent.getParcelableExtra<Card>("card")
        if (card.cardNumber.isNotBlank()) {
            etCardNo.setText(card.cardNumber)
            etCvv.setText(card.cvv)
            etExpDate.setText(card.expDate)
            etName.setText(card.cardNumber)
            etCardNo.isEnabled = false
            etCvv.isEnabled = false
            etExpDate.isEnabled = false
            etName.isEnabled = false
            save.visibility = View.GONE
        }
    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        dismissProgressBar()
        showToast(if (apiResponse.status) {
            setResult(Activity.RESULT_OK)
            finish()
            apiResponse.message
        } else apiResponse.error.description)
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
    }

    private fun chooseDOB(it: View) {

        val calendar = Calendar.getInstance()

        val dpDialog = DatePickerDialog(it.context, R.style.TimePickerTheme, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            exp = (month + 1).toString() + "/" + year.toString()
            etExpDate.setText(exp)
            expYear = year
            expMonth = month + 1

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        dpDialog.datePicker.minDate = calendar.timeInMillis
        dpDialog.show()
    }

}
