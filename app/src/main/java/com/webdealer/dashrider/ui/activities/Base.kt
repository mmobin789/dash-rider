package com.webdealer.dashrider.ui.activities

import android.Manifest
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.KeyEvent
import android.view.Window
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.wang.avi.AVLoadingIndicatorView
import com.webdealer.dashrider.R
import com.webdealer.dashrider.ui.fragments.BaseUI
import com.webdealer.dashrider.utils.AppStorage
import com.webdealer.dashrider.viewmodel.DashRiderImpl
import kotlinx.android.synthetic.main.toolbar.*
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.concurrent.TimeUnit

abstract class Base : AppCompatActivity() {
    companion object {
        var fcmData: Bundle? = null
        lateinit var dashRiderImpl: DashRiderImpl
        fun getLinearLayoutManager(context: Context) = LinearLayoutManager(context)
        fun delay(seconds: Long, runnable: Runnable) {
            Handler().postDelayed(runnable, TimeUnit.SECONDS.toMillis(seconds))
        }

        fun showProgressBar() {
            try {
                progressBar.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun dismissProgressBar() {
            try {
                progressBar.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private lateinit var progressBar: Dialog
        private fun createProgressBar(context: Context): Dialog {

            val dialog = Dialog(context)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setOnKeyListener { _, _, p2 ->
                if (p2.keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()

                }
                true
            }
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val relativeLayout = RelativeLayout(context)
            val avLoadingIndicatorView = AVLoadingIndicatorView(context)
            avLoadingIndicatorView.setIndicatorColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            //avLoadingIndicatorView.setIndicator(BallPulseIndicator())
            val params = RelativeLayout.LayoutParams(150, 150)
            params.addRule(RelativeLayout.CENTER_IN_PARENT)
            avLoadingIndicatorView.layoutParams = params
            relativeLayout.addView(avLoadingIndicatorView)
            dialog.setContentView(relativeLayout)
            return dialog


        }

        fun hasLocationPermission(base: Base): Boolean {
            var permission = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permission = ActivityCompat.checkSelfPermission(base, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(base, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                if (!permission) {

                    base.requestPermissions(arrayOf(
                            Manifest
                                    .permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
                    ), 3)
                }
            }
            return permission
        }

        fun zoomToMyLocation(googleMap: GoogleMap, lat: Double, lng: Double) {

            val latLng = LatLng(lat, lng)
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12f)
            googleMap.animateCamera(cameraUpdate)

        }


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).build())
        progressBar = createProgressBar(this)
        AppStorage.init(this)
        dashRiderImpl = ViewModelProviders.of(this).get(DashRiderImpl::class.java)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    fun setToolbarTitle(title: String) {
        titleTB.text = title
    }

    fun showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    fun setFragment(baseUI: BaseUI) = supportFragmentManager.beginTransaction().replace(R.id.container, baseUI).commit()
}