package com.webdealer.dashrider.ui.activities

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import com.webdealer.dashrider.R
import kotlinx.android.synthetic.main.toolbar.*

class TrackRide : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_ride)
        setToolbarTitle("Book a Ride")
        cancel.visibility = View.VISIBLE
    }

    fun contactDriverDialog(v: View) {
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.contact_driver_dialog)
        dialog.show()
    }
}
