package com.webdealer.dashrider.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.ui.activities.ChangePassword
import com.webdealer.dashrider.ui.activities.Login
import com.webdealer.dashrider.ui.activities.Rates
import com.webdealer.dashrider.utils.AppStorage
import kotlinx.android.synthetic.main.fragment_settings.*

class Settings : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        changePassword.setOnClickListener {
            startActivity(Intent(it.context, ChangePassword::class.java))
        }
        ratesUI.setOnClickListener {
            startActivity(Intent(it.context, Rates::class.java))
        }
        signOut.setOnClickListener {
            AppStorage.clearSession()
            base.onBackPressed()
            val login = Intent(it.context, Login::class.java)
            login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            startActivity(login)
        }
    }

    companion object {


        fun newInstance() = Settings()
    }
}