package com.webdealer.dashrider.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.ui.activities.Verification
import kotlinx.android.synthetic.main.verify_phone.*

class VerifyPhone : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.verify_phone, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val verification = base as Verification
        proceed.setOnClickListener {
            val phone = etPhone.text.toString()
            if (phone.isBlank())
                showToast("Phone Required")
            else {

                val fullNumber = ccp.selectedCountryCodeWithPlus + phone
                //   verification.signUp(fullNumber)


            }
        }
    }


    companion object {
        fun newInstance() =
                VerifyPhone()


    }

}