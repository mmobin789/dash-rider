package com.webdealer.dashrider.contracts

import com.webdealer.dashrider.models.ApiResponse

interface OnApiListener : OnApiErrorListener {
    fun onApiResponse(apiResponse: ApiResponse)
}