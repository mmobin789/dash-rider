package com.webdealer.dashrider.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.webdealer.dashrider.ui.activities.Base

abstract class BaseUI : Fragment() {

    protected lateinit var base: Base
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = activity as Base
    }

    fun showToast(msg: String) = Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
}