package com.webdealer.dashrider.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.ui.activities.Base
import com.webdealer.dashrider.ui.adapters.ScheduledRidesAdapter
import kotlinx.android.synthetic.main.scheduled_ride_or_history.*

class RideHistory : BaseUI() {
    companion object {
        private var myRides: RideHistory? = null
        fun newInstance(): RideHistory {
            if (myRides == null)
                myRides = RideHistory()
            return myRides!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.scheduled_ride_or_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = Base.getLinearLayoutManager(context!!)
        rv.adapter = ScheduledRidesAdapter(true)
    }
}