package com.webdealer.dashrider.ui.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.ui.activities.RideHistoryDetails
import com.webdealer.dashrider.ui.activities.ScheduledRideDetails
import kotlinx.android.synthetic.main.ride_row.*

class ScheduledRidesAdapter(private val isHistory: Boolean) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(null, LayoutInflater.from(parent.context).inflate(R.layout.ride_row, parent, false))
        vh.containerView.setOnClickListener {
            val detail = if (isHistory)
                RideHistoryDetails::class.java
            else ScheduledRideDetails::class.java
            parent.context.startActivity(Intent(parent.context, detail))
        }
        if (isHistory)
            vh.priceTV.visibility = View.VISIBLE
        return vh
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

}