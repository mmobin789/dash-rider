package com.webdealer.dashrider.ui.activities

import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import com.google.android.gms.maps.SupportMapFragment
import com.google.gson.Gson
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.RideBooking
import com.webdealer.dashrider.utils.AppStorage
import com.webdealer.dashrider.utils.Utils
import kotlinx.android.synthetic.main.activity_go.*
import kotlinx.android.synthetic.main.go_ui.*

class Go : Base(), OnApiListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_go)
        init()
    }

    private fun getCountryName(latitude: Double, longitude: Double): String {
        val geoCoder = Geocoder(this)
        var country = "N/A"
        try {
            val addresses = geoCoder.getFromLocation(latitude, longitude, 1)
            country = addresses[0].countryName
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return country
    }

    private fun init() {
        setToolbarTitle("Booking")
        cardTV.text = getCardInfo()
        val map = map as SupportMapFragment
        map.getMapAsync {

        }
        visaUI.setOnClickListener {
            startActivityForResult(Intent(it.context, Wallet::class.java), 11)
        }

        val rideBooking = Gson().fromJson(intent.getStringExtra("rideBooking"), RideBooking::class.java)

        go.setOnClickListener {
            showProgressBar()
            dashRiderImpl.bookRide(this, rideBooking, this)
        }
        val country = getCountryName(rideBooking.picklat.toDouble(), rideBooking.picklng.toDouble())
        fromTV.text = rideBooking.localPickUpAddress
        fromCTV.text = country
        toTV.text = rideBooking.localDropOffAddress
        toCTV.text = country

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 11 && resultCode == Activity.RESULT_OK)
            cardTV.text = getCardInfo()
    }

    private fun getCardInfo(): String {
        val cardsList = AppStorage.loadPaymentInfo()
        val index = if (cardsList.size == 1)
            cardsList.size - 1
        else cardsList.size - 2
        val card = cardsList[index]
        return "${card.name} ${Utils.getMaskedCardNumber(card.cardNumber)}"
    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        dismissProgressBar()

        showToast(if (apiResponse.status) {
            apiResponse.message
        } else apiResponse.error.description)
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        showToast(error)
    }
}
