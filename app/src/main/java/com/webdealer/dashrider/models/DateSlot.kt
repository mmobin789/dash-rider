package com.webdealer.dashrider.models

import java.util.*

class DateSlot(val month: String, val dayOfWeek: String, val dayOfMonth: Int, var isSet: Boolean) {
    fun toStringDate(): String {
        val calendar = Calendar.getInstance()
        return "$dayOfWeek, $month $dayOfMonth, ${calendar.get(Calendar.YEAR)}"
    }
}