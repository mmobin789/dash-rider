package com.webdealer.dashrider.models

data class DriverInfo(val lastTripEarnings: String, val lastTripMiles: String) {
    val isRider = "1"
    var rideID = "-1"
    val email = "-1"
    var currentlat = "-1"
    var currentlng = "-1"
}