package com.webdealer.dashrider.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.Auth
import com.webdealer.dashrider.models.Card
import com.webdealer.dashrider.ui.adapters.CardsAdapter
import com.webdealer.dashrider.utils.AppStorage
import kotlinx.android.synthetic.main.activity_wallet.*
import kotlinx.android.synthetic.main.toolbar.*

class Wallet : Base(), OnApiListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)


        init()
    }

    private fun init() {
        setToolbarTitle("Wallet")
        back.visibility = View.VISIBLE

        back.setOnClickListener {
            onBackPressed()
        }


        rv.layoutManager = LinearLayoutManager(this)


        getCards()

    }

    private fun getCards() {
        showProgressBar()
        dashRiderImpl.getCards(this, Auth(AppStorage.getUser().email, ""), this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 44 && resultCode == Activity.RESULT_OK)
            getCards()
    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        dismissProgressBar()
        var list = apiResponse.cardsList
        if (list == null) {
            list = mutableListOf()

        }
        addCardOption(list)


    }

    private fun addCardOption(cardsList: MutableList<Card>) {
        cardsList.add(Card("", "", "", "Add Credit/Debit Card"))
        val adapter = CardsAdapter(this, cardsList)
        rv.adapter = adapter
        AppStorage.savePaymentInfo(cardsList)
        setResult(Activity.RESULT_OK)
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        addCardOption(mutableListOf())
    }
}
