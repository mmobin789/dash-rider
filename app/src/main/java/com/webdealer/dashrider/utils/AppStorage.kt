package com.webdealer.dashrider.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.webdealer.dashrider.models.Card
import com.webdealer.dashrider.models.User

object AppStorage {
    private lateinit var prefs: SharedPreferences
    private val gson = Gson()
    private const val keyUser = "user"
    fun init(context: Context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveUser(user: User) {
        val s = gson.toJson(user)
        prefs.edit().putString(keyUser, s).apply()
    }

    fun getFCM() = prefs.getString("fcm", "")

    fun setFCM(token: String) = prefs.edit().putString("fcm", token).apply()

    fun getUser() = gson.fromJson(prefs.getString(keyUser, ""), User::class.java)

    fun clearSession() = prefs.edit().clear().apply()

    fun isLoggedIn() = prefs.getString(keyUser, "").isNotBlank()

    fun savePaymentInfo(cardsList: List<Card>) {
        val gson = Gson()
        val listType = object : TypeToken<List<Card>>() {

        }.type
        val json = gson.toJson(cardsList, listType)
        prefs.edit().putString(getUser().email, json).apply()

    }

    fun loadPaymentInfo(): List<Card> {

        val listType = object : TypeToken<List<Card>>() {

        }.type
        val json = prefs.getString(getUser().email, "")
        var list: List<Card>? = gson.fromJson<List<Card>>(json, listType)
        if (list == null) {
            list = mutableListOf()
            list.add(Card("", "", "", "Add Credit/Debit Card"))
        }
        return list
    }
}