package com.webdealer.dashrider.contracts

interface OnInstagramAccessListener {
    fun onInstagramAccessGranted(accessToken: String)
}