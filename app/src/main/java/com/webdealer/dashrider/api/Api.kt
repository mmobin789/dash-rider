package com.webdealer.dashrider.api

import android.content.Context
import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.webdealer.dashrider.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

object Api {

    private const val baseUrl = "http://52.35.158.11/dashweb/web/index.php/releaser/"
    private var retrofit: Retrofit? = null

    fun getWebApi(): WebService {
        if (retrofit == null) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
            retrofit = Retrofit.Builder().baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build()
        }

        return retrofit!!.create(WebService::class.java)
    }

    private fun getImageRequestBody(context: Context, file: File, key: String): MultipartBody.Part {
        val compressed = Utils.getCompressedFile(context, file)!!
        Log.d("compressedImage", compressed.path)
        val mFile = RequestBody.create(MediaType.parse("image/*"), compressed)
        return MultipartBody.Part.createFormData(key, compressed.name, mFile)
    }

    private fun getTextRequestBody(data: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), data)
    }

    fun stripe(context: Context, card: Card, tokenCallback: TokenCallback) =
            Stripe(context, "pk_test_csplrS1gcbgGkZRlfvdqEx1G").createToken(card, tokenCallback)


}