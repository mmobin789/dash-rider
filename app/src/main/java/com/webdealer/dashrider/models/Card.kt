package com.webdealer.dashrider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Card(@SerializedName("card_number") val cardNumber: String
                , @SerializedName("exp_date") val expDate: String,
                val cvv: String, @SerializedName("holder_name") val name: String) : Parcelable