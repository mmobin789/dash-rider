package com.webdealer.dashrider.ui.adapters

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.Auth
import com.webdealer.dashrider.models.Card
import com.webdealer.dashrider.ui.activities.Base
import com.webdealer.dashrider.ui.activities.Base.Companion.dashRiderImpl
import com.webdealer.dashrider.ui.activities.Base.Companion.dismissProgressBar
import com.webdealer.dashrider.ui.activities.Base.Companion.showProgressBar
import com.webdealer.dashrider.ui.activities.Payment
import com.webdealer.dashrider.utils.AppStorage
import com.webdealer.dashrider.utils.Utils.getMaskedCardNumber
import kotlinx.android.synthetic.main.add_card_row.*
import kotlinx.android.synthetic.main.delete_dialog.*

class CardsAdapter(private val base: Base, private val list: MutableList<Card>) : RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(null, LayoutInflater.from(parent.context).inflate(R.layout.add_card_row, parent, false))
        vh.containerView.setOnClickListener {
            val cardDetails = Intent(parent.context, Payment::class.java)
            cardDetails.putExtra("card", list[vh.adapterPosition])
            base.startActivityForResult(cardDetails, 44)
        }
        vh.containerView.setOnLongClickListener {
            val lastIndex = itemCount - 1
            val position = vh.adapterPosition
            val cardNumber = list[position].cardNumber
            if (cardNumber.isNotBlank() && position != lastIndex) {
                showDeleteDialog(it.context, vh.adapterPosition, cardNumber)
            }

            true
        }

        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun showDeleteDialog(context: Context, position: Int, cardNumber: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.delete_dialog)
        dialog.delete.setOnClickListener {
            val auth = Auth(AppStorage.getUser().email, "")
            auth.cardNumber = cardNumber
            showProgressBar()
            dashRiderImpl.deleteCard(base, auth, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    if (apiResponse.status) {
                        dialog.dismiss()
                        base.showToast("Success")
                        list.removeAt(position)
                        notifyItemRemoved(position)
                    } else base.showToast(apiResponse.error.description)
                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    base.showToast(error)
                }
            })
        }
        dialog.show()
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val card = list[position]
        val data = "${card.name} ${getMaskedCardNumber(card.cardNumber)}"
        holder.cardTV.text = data
    }


}