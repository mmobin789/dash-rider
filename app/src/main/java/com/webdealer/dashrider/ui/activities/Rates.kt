package com.webdealer.dashrider.ui.activities

import android.os.Bundle
import android.view.View
import com.webdealer.dashrider.R
import kotlinx.android.synthetic.main.toolbar.*

class Rates : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rates)
        setToolbarTitle("Rates")
        back.visibility = View.VISIBLE
        back.setOnClickListener {
            onBackPressed()
        }
    }
}
