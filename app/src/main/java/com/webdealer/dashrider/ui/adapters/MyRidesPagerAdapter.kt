package com.webdealer.dashrider.ui.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.webdealer.dashrider.ui.fragments.BaseUI
import com.webdealer.dashrider.ui.fragments.RideHistory
import com.webdealer.dashrider.ui.fragments.ScheduledRides

class MyRidesPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val title = arrayOf("scheduled", "history")
    override fun getItem(position: Int): BaseUI {
        return when (position) {
            1 -> {
                RideHistory.newInstance()
            }
            else -> {
                ScheduledRides.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return title[position]
    }
}