package com.webdealer.dashrider.ui.activities

import android.os.Bundle
import com.webdealer.dashrider.R

class BookingPackage : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_package)
        setToolbarTitle("Booking Time")
    }
}
