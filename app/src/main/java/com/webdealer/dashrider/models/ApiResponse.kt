package com.webdealer.dashrider.models

data class ApiResponse(val status: Boolean, val data: Data, val error: Error, val message: String, val cardsList: MutableList<Card>?
                       , val currentlat: String, val currentlng: String) {
    data class Data(val user: User)
    data class Error(val code: Int, val description: String)


}