package com.webdealer.dashrider.ui.adapters

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.webdealer.dashrider.ui.fragments.BaseUI
import com.webdealer.dashrider.ui.fragments.PinVerification
import com.webdealer.dashrider.ui.fragments.VerifyPhone

class SignUpPager(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    fun setPhoneNumber(phone: String) {
        val bundle = Bundle()
        bundle.putString("phone", phone)
        getItem(1).arguments = bundle
    }

    override fun getItem(position: Int): BaseUI {
        return when (position) {
            0 -> VerifyPhone.newInstance()
            else -> {
                PinVerification.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }
}