package com.webdealer.dashrider.models

data class User(val name: String, val email: String,
                val phone: String, var status: String, val paymentVerified: String
)