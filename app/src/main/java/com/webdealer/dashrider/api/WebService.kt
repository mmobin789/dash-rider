package com.webdealer.dashrider.api

import com.webdealer.dashrider.models.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface WebService {

    @POST("login")
    fun login(@Body auth: Auth): Observable<ApiResponse>

    @POST("register")
    fun register(@Body auth: Auth): Observable<ApiResponse>

    @POST("verify")
    fun phoneVerify(@Body auth: Auth): Observable<ApiResponse>

    @POST("resendcode")
    fun resendCode(@Body auth: Auth): Observable<ApiResponse>

    @POST("fcmtoken")
    fun fcm(@Body auth: Auth): Observable<ApiResponse>

    @POST("paymentverification")
    fun addPaymentInfo(@Body paymentInfo: PaymentInfo): Observable<ApiResponse>

    @POST("paymentverificationlist")
    fun getCardsList(@Body auth: Auth): Observable<ApiResponse>

    @POST("deletecard")
    fun deleteCard(@Body auth: Auth): Observable<ApiResponse>

    @POST("bookride")
    fun bookRide(@Body rideBooking: RideBooking): Observable<ApiResponse>

    @POST("trackdriver")
    fun realTimeTrackDriver(@Body driverInfo: DriverInfo): Observable<ApiResponse>

    @GET("${Instagram.insta_BASE_URL}v1/users/self")
    fun getInstagramUser(@Query("access_token") accessToken: String): Observable<Instagram>
}