package com.webdealer.dashrider.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.google.android.gms.maps.SupportMapFragment
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnListItemClickListener
import com.webdealer.dashrider.models.DateSlot
import com.webdealer.dashrider.models.TimeSlot
import com.webdealer.dashrider.ui.adapters.DateSlotAdapter
import com.webdealer.dashrider.ui.adapters.TimeSlotAdapter
import kotlinx.android.synthetic.main.activity_ride_later.*
import kotlinx.android.synthetic.main.cars_ui.*
import java.text.SimpleDateFormat
import java.util.*

class RideLater : Base() {
    private var carType = "GoDash"
    private var date = ""
    private var time = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride_later)
        setToolbarTitle("Ride Later")
        init()
    }

    private fun getTimeSlots(): List<TimeSlot> {
        val timeSlots = resources.getStringArray(R.array.times)
        val timeSlotList = mutableListOf<TimeSlot>()
        for (i in 0 until timeSlots.size) {
            val isSet = i == 1
            val timeSlot = TimeSlot(timeSlots[i].trim(), isSet)
            timeSlotList.add(timeSlot)

        }
        return timeSlotList
    }

    private fun carUI() {
        carGo.setOnClickListener {
            carType = "Go"
            carGoIV.setImageResource(R.drawable.car_blue)
            carDashIV.setImageResource(R.drawable.car_gray)
            carVipIV.setImageResource(R.drawable.car_gray)
            goLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.btn))
            goTV.setTextColor(ContextCompat.getColor(it.context, R.color.btn))
            dashLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goDashTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
            vipLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            vipTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
        }
        carVip.setOnClickListener {
            carType = "GoVip"
            carVipIV.setImageResource(R.drawable.car_blue)
            carGoIV.setImageResource(R.drawable.car_gray)
            carDashIV.setImageResource(R.drawable.car_gray)
            vipLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.btn))
            vipTV.setTextColor(ContextCompat.getColor(it.context, R.color.btn))
            goLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goDashTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
            dashLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
        }
        carDash.setOnClickListener {
            carType = "GoDash"
            carDashIV.setImageResource(R.drawable.car_blue)
            carGoIV.setImageResource(R.drawable.car_gray)
            carVipIV.setImageResource(R.drawable.car_gray)
            dashLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.btn))
            goDashTV.setTextColor(ContextCompat.getColor(it.context, R.color.btn))
            goLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            vipTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
            vipLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
        }
    }

    private fun init() {
        val map = map as SupportMapFragment
        map.getMapAsync {
            it.uiSettings.isZoomControlsEnabled = true
            it.uiSettings.isZoomGesturesEnabled = true
        }
        var layoutManager = getLinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rvTime.layoutManager = layoutManager
        layoutManager = getLinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rvDate.layoutManager = layoutManager
        initDatesAdapter()
        initTimeAdapter()
        next.setOnClickListener {
            val data = Intent()
            data.putExtra("time", getUnixTime())
            setResult(Activity.RESULT_OK, data) // send time and date along to home UI
            finish()
        }
        carUI()

    }

    private fun getUnixTime(): String {

        val timeStamp = "$date $time"
        val sdf = SimpleDateFormat("EEE, MMM dd, yyyy h:mm a", Locale.getDefault())
        showToast(sdf.format(sdf.parse(timeStamp)))
        val unixTime = (sdf.parse(timeStamp).time / 1000L).toString()
        Log.i("unixTime", unixTime)
        return unixTime
    }

    private fun initDatesAdapter() {

        val currentTimeC = Calendar.getInstance()
        val dates = mutableListOf<DateSlot>()
        val monthFormat = SimpleDateFormat("MMM", Locale.getDefault())
        for (i in 0..7) {
            val isSet = i == 0
            val dateSlot = DateSlot(monthFormat.format(currentTimeC.time), currentTimeC.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault()), currentTimeC.get(Calendar.DAY_OF_MONTH), isSet)
            dates.add(dateSlot)
            currentTimeC.add(Calendar.DATE, 1)


        }
        var lastCheckedPosition = 0
        date = dates[0].toStringDate()
        rvDate.adapter = DateSlotAdapter(dates, object : OnListItemClickListener {
            override fun onListItemClicked(position: Int) {
                dates[lastCheckedPosition].isSet = false
                rvDate.adapter.notifyItemChanged(lastCheckedPosition)
                dates[position].isSet = true
                rvDate.adapter.notifyItemChanged(position)
                lastCheckedPosition = position
                date = dates[position].toStringDate()
                // showToast(date)
            }
        })


    }


    private fun initTimeAdapter() {
        var lastCheckedPosition = 1
        val timeSlots = getTimeSlots()
        time = timeSlots[1].getStartTime()
        rvTime.adapter = TimeSlotAdapter(timeSlots, object : OnListItemClickListener {
            override fun onListItemClicked(position: Int) {
                timeSlots[lastCheckedPosition].isSet = false
                rvTime.adapter.notifyItemChanged(lastCheckedPosition)
                timeSlots[position].isSet = true
                rvTime.adapter.notifyItemChanged(position)
                lastCheckedPosition = position
                time = timeSlots[position].getStartTime()
                // showToast(time)
            }
        })
    }
}
