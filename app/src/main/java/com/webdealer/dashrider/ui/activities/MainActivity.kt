package com.webdealer.dashrider.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.Auth
import com.webdealer.dashrider.ui.fragments.*
import com.webdealer.dashrider.utils.AppStorage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : Base(), OnApiListener {
    lateinit var baseUI: BaseUI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {


        val user = AppStorage.getUser()

        if (user.status == "0") {
            val verify = Intent(this, Verification::class.java)
            verify.putExtra("loggedIn", true)
            startActivity(verify)
            finish()
        }
        val navClickEvent = View.OnClickListener {
            baseUI = when (it.id) {
                help.id -> {
                    ContactUs.newInstance()
                }
                settings.id -> {
                    Settings.newInstance()
                }
                rides.id -> {
                    MyRides.newInstance()
                }

                packageBuy.id -> {
                    setToolbarTitle("Choose your package")
                    BuyPackage.newInstance()
                }

                else -> {
                    Home.newInstance()
                }
            }
            drawerL.closeDrawer(Gravity.START, true)
            setFragment()
        }
        setToolbarTitle("Dash")
        baseUI = Home.newInstance()
        setFragment()
        home.setOnClickListener(navClickEvent)
        help.setOnClickListener(navClickEvent)
        rides.setOnClickListener(navClickEvent)
        wallet.setOnClickListener { startActivity(Intent(it.context, Wallet::class.java)) }
        settings.setOnClickListener(navClickEvent)
        packageBuy.setOnClickListener(navClickEvent)
        drawerIV.visibility = View.VISIBLE
        drawerIV.setOnClickListener {

            drawerL.openDrawer(Gravity.START)
        }

        delay(1, Runnable {
            dashRiderImpl.fcm(this, Auth(user.email, ""), this)
        })


    }


    override fun onApiResponse(apiResponse: ApiResponse) {
        if (apiResponse.status)
            showToast(apiResponse.message)


    }

    override fun onApiError(error: String) {
        showToast(error)
    }

    private fun setFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, baseUI).commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (baseUI is Home)
            baseUI.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (baseUI is Home)
            baseUI.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun home(): Boolean {
        return if (baseUI is Home) {
            val home = baseUI as Home
            if (home.dropOffMenu) {
                home.dropOffMenu = false
                home.pickUpMenu()
                false
            } else
                true
        } else
            true
    }

    override fun onBackPressed() {

        if (home())
            super.onBackPressed()
    }
}
