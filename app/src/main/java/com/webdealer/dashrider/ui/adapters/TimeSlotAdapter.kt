package com.webdealer.dashrider.ui.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnListItemClickListener
import com.webdealer.dashrider.models.TimeSlot
import kotlinx.android.synthetic.main.time_row.*

class TimeSlotAdapter(private val list: List<TimeSlot>, private val onListItemClickListener: OnListItemClickListener) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(onListItemClickListener, LayoutInflater.from(parent.context).inflate(R.layout.time_row, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val timeSlot = list[position]
        holder.timeTV.text = timeSlot.timeSlot
        if (timeSlot.isSet) {
            holder.dropDown.visibility = View.VISIBLE
            holder.dropUP.visibility = View.VISIBLE
            holder.timeTV.setTextColor(Color.BLACK)
        } else {
            holder.dropDown.visibility = View.INVISIBLE
            holder.dropUP.visibility = View.INVISIBLE
            holder.timeTV.setTextColor(Color.LTGRAY)
        }
    }
}