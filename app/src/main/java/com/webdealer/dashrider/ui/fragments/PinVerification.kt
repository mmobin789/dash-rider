package com.webdealer.dashrider.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.Auth
import com.webdealer.dashrider.ui.activities.Base.Companion.dashRiderImpl
import com.webdealer.dashrider.ui.activities.Base.Companion.dismissProgressBar
import com.webdealer.dashrider.ui.activities.Base.Companion.showProgressBar
import com.webdealer.dashrider.ui.activities.MainActivity
import com.webdealer.dashrider.utils.AppStorage
import kotlinx.android.synthetic.main.numpad.*
import kotlinx.android.synthetic.main.verify_pin.*

class PinVerification : BaseUI(), OnApiListener {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.verify_pin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        numPad()
        proceed.setOnClickListener {
            verifyPhone()
        }
        setCodeUI()

        rc.setOnClickListener {
            showProgressBar()
            val auth = Auth(AppStorage.getUser().email, "")
            dashRiderImpl.resendCode(this, auth, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()

                    showToast(if (apiResponse.status)
                        apiResponse.message
                    else apiResponse.error.description)

                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    showToast(error)
                }
            })
        }
    }

    private fun verifyPhone() {
        val user = AppStorage.getUser()
        val auth = Auth(user.email, "")
        val code = digit1.text.toString() + digit2.text.toString() + digit3.text.toString() + digit4.text.toString()
        if (code.isNotBlank() && code.length == 4) {
            auth.code = code
            showProgressBar()
            dashRiderImpl.verifyPhone(this, auth, this)
        } else
            showToast("Invalid Code")
    }

    private fun setCodeUI() {
        val phone = AppStorage.getUser().phone
        var code = "Enter the  4 - digit code sent to you at " + phone.substring(0, 4)
        val x = StringBuilder()
        for (i in 5..phone.length) {
            x.append("X")
        }
        code += x.toString()
        codeTV.text = code

    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        dismissProgressBar()
        if (apiResponse.status) {
            // success registered
            val user = AppStorage.getUser()
            user.status = "1"
            AppStorage.saveUser(user)
            showToast("Sign Up Success.")
            startActivity(Intent(context, MainActivity::class.java))
            base.finish()
        } else showToast(apiResponse.error.description)
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        showToast(error)
    }


    private fun numPad() {

        val numPadClick = View.OnClickListener { v ->
            when (v.id) {
                11 -> setDisplay("0")
                12 -> backSpace()
                else -> setDisplay(v.id.toString())
            }

        }

        val rows = numpad.childCount
        for (i in 0 until rows) {
            val row = numpad.getChildAt(i) as LinearLayout
            val tvCount = row.childCount
            for (d in 0 until tvCount) {
                val tv = row.getChildAt(d)
                val id = when (i) {
                    0 -> d + 1
                    1 -> d + 4
                    2 -> d + 7
                    else -> d + 10
                }
                tv.id = id
                tv.setOnClickListener(numPadClick)
            }
        }
    }

    private fun setDisplay(number: String) {

        when {
            digit1.length() == 0 -> digit1.setText(number)
            digit2.length() == 0 -> digit2.setText(number)
            digit3.length() == 0 -> digit3.setText(number)
            digit4.length() == 0 -> digit4.setText(number)
        }
    }

    private fun backSpace() {
        when {
            digit4.length() > 0 -> digit4.setText("")
            digit3.length() > 0 -> digit3.setText("")
            digit2.length() > 0 -> digit2.setText("")
            digit1.length() > 0 -> digit1.setText("")
        }
    }


    companion object {
        fun newInstance() = PinVerification()
    }
}