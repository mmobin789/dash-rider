package com.webdealer.dashrider.ui.activities

import android.os.Bundle
import android.view.View
import com.webdealer.dashrider.R
import kotlinx.android.synthetic.main.toolbar.*

class Billing : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bill)
        setToolbarTitle("Choose Billing")
        back.visibility = View.VISIBLE
        back.setOnClickListener {
            onBackPressed()
        }
    }
}
