package com.webdealer.dashrider.ui.fragments

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.*
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.models.ApiResponse
import com.webdealer.dashrider.models.RideBooking
import com.webdealer.dashrider.ui.activities.Base
import com.webdealer.dashrider.ui.activities.Base.Companion.dashRiderImpl
import com.webdealer.dashrider.ui.activities.Base.Companion.hasLocationPermission
import com.webdealer.dashrider.ui.activities.Base.Companion.zoomToMyLocation
import com.webdealer.dashrider.ui.activities.Go
import com.webdealer.dashrider.ui.activities.RideLater
import kotlinx.android.synthetic.main.cars_ui.*
import kotlinx.android.synthetic.main.location_from_to.*
import kotlinx.android.synthetic.main.pickup_dropoff_location.*


class Home : BaseUI() {
    var dropOffMenu = false
    private var pickUpPlace: String? = null
    private lateinit var rideBooking: RideBooking
    private lateinit var googleMap: GoogleMap

    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var carType = "DashGo"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.pickup_dropoff_location, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rideBooking = RideBooking()
        base = activity as Base
        pickUpMenu()
        setupMap()
        init()

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            showCurrentLocation()
        else showToast("Location Permission Required to show your current location.")
    }


    private fun setupMap() {
        map.onCreate(null)
        map.getMapAsync {
            map.onResume()
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isZoomControlsEnabled = true
            googleMap = it
            showCurrentLocation()


        }
    }

    @SuppressLint("MissingPermission")
    private fun showCurrentLocation() {
        if (hasLocationPermission(base)) {
            googleMap.isMyLocationEnabled = true
            getUserLocation()
        }
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    private object locationCallback : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                val lat = p0.lastLocation.latitude
                val lng = p0.lastLocation.longitude
                zoomToMyLocation(home!!.googleMap, lat, lng)

            }
        }

    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(base)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 20000
        locationRequest.fastestInterval = 10000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient!!.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun dropOffMenu(isRideLater: Boolean, isPassenger: Boolean, rideLaterData: Intent?) {
        to.visibility = View.VISIBLE
        pickUpAddress.visibility = View.VISIBLE
        dropOffAddress.visibility = View.VISIBLE
        cars.visibility = View.GONE

        base.setToolbarTitle("Drop Off Location")
        dropOffMenu = true
        dropOffTV.visibility = View.VISIBLE
        line.visibility = View.VISIBLE
        rideNow.text = "Add Drop Off"
        rideNow.setOnClickListener {
            //    if (isRideLater && isPassenger) // if passenger

            //    startActivity(Intent(it.context, BookingPackage::class.java))
            // else if (isRideLater && !isPassenger) {   // its package and drop off is required for it
            dropOffTV.performClick()

        }
        rideLater.text = "Skip"
        rideLater.setBackgroundResource(R.drawable.skip_btn)
        rideLater.setOnClickListener {
            if (isRideLater && rideLaterData != null) {
                rideBooking.isRideNow = "0"
                rideBooking.pickupDate = rideLaterData.getStringExtra("time")
            } else rideBooking.isRideNow = "1"

            val go = Intent(it.context, Go::class.java)
            go.putExtra("rideBooking", rideBooking.toJsonString())
            startActivity(go)
        }
        if (!isRideLater)
            rideLater.visibility = View.VISIBLE
        else if (isRideLater && !isPassenger) rideLater.visibility = View.GONE

        dropOffTV.setOnClickListener {
            pickUpPlace = ""
            setLocationFromGooglePicker()
        }
    }

    fun pickUpMenu() {
        to.visibility = View.GONE
        pickUpAddress.visibility = View.VISIBLE
        dropOffAddress.visibility = View.GONE
        cars.visibility = View.VISIBLE
        base.setToolbarTitle("Pick Up Location")
        dropOffMenu = false
        line.visibility = View.GONE
        dropOffTV.visibility = View.GONE
        rideNow.text = "Ride Now"
        rideLater.text = "Ride Later"
        rideLater.setBackgroundResource(R.drawable.black_btn_left)
        rideNow.setOnClickListener {
            rideBooking.carType = carType
            if (!pickUpPlace.isNullOrBlank()) {
                dropOffMenu(false, true, null)

            } else showToast("Choose Pickup Place")
        }
        rideLater.visibility = View.VISIBLE
        rideLater.setOnClickListener {
            rideBooking.carType = carType
            if (!pickUpPlace.isNullOrBlank()) {
                val rideLater = Intent(it.context, RideLater::class.java)
                rideLater.putExtra("rideBooking", rideBooking.toJsonString())
                base.startActivityForResult(rideLater, 44)
            } else showToast("Choose Pickup Place")
        }
        pickUpTV.setOnClickListener {
            pickUpPlace = "p"
            setLocationFromGooglePicker()
        }
        search.setOnClickListener {
            pickUpTV.performClick()
        }
    }

    private fun setLocationFromGooglePicker() {
        val builder = PlacePicker.IntentBuilder()
        base.startActivityForResult(builder.build(base), 12)

    }

    private fun init() {
        carUI()

        if (base.intent.hasExtra("rideID")) {
            Base.fcmData = Bundle()
            Base.fcmData!!.putString("rideID", base.intent.getStringExtra("rideID"))
            trackDriver()

        }

    }

    private fun showDriverOnMap(lat: Double, lng: Double) {
        googleMap.clear()
        val markerOptions = MarkerOptions()
        markerOptions.position(LatLng(lat, lng))
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_marker))
        googleMap.addMarker(markerOptions)
    }

    private fun trackDriver() {
        dashRiderImpl.realTimeDriverLocation(this, object : OnApiListener {
            override fun onApiResponse(apiResponse: ApiResponse) {
                if (apiResponse.status) {
                    showDriverOnMap(apiResponse.currentlat.toDouble(), apiResponse.currentlng.toDouble())
                }
                Base.delay(10, Runnable {
                    trackDriver()
                })

            }

            override fun onApiError(error: String) {

            }
        })
    }

    private fun carUI() {
        carGo.setOnClickListener {
            carType = "Go"
            carGoIV.setImageResource(R.drawable.car_blue)
            carDashIV.setImageResource(R.drawable.car_gray)
            carVipIV.setImageResource(R.drawable.car_gray)
            goLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.btn))
            goTV.setTextColor(ContextCompat.getColor(it.context, R.color.btn))
            dashLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goDashTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
            vipLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            vipTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
        }
        carVip.setOnClickListener {
            carType = "DashX"
            carVipIV.setImageResource(R.drawable.car_blue)
            carGoIV.setImageResource(R.drawable.car_gray)
            carDashIV.setImageResource(R.drawable.car_gray)
            vipLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.btn))
            vipTV.setTextColor(ContextCompat.getColor(it.context, R.color.btn))
            goLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goDashTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
            dashLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
        }
        carDash.setOnClickListener {
            carType = "DashGo"
            carDashIV.setImageResource(R.drawable.car_blue)
            carGoIV.setImageResource(R.drawable.car_gray)
            carVipIV.setImageResource(R.drawable.car_gray)
            dashLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.btn))
            goDashTV.setTextColor(ContextCompat.getColor(it.context, R.color.btn))
            goLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            vipTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
            vipLine.setBackgroundColor(ContextCompat.getColor(it.context, R.color.gray))
            goTV.setTextColor(ContextCompat.getColor(it.context, R.color.black))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 44 && resultCode == RESULT_OK) {
            dropOffMenu(true, true, data)
        } else if (requestCode == 12 && resultCode == RESULT_OK) {
            val place = PlacePicker.getPlace(base, data)
            if (!pickUpPlace.isNullOrBlank()) {
                pickUpPlace = place.address.toString()
                pickUpAddress.text = place.address
                rideBooking.picklat = place.latLng.latitude.toString()
                rideBooking.picklng = place.latLng.longitude.toString()
                rideBooking.localPickUpAddress = place.address.toString()
            } else {
                dropOffAddress.text = place.address
                rideBooking.droplat = place.latLng.latitude.toString()
                rideBooking.droplng = place.latLng.longitude.toString()
                rideBooking.localDropOffAddress = place.address.toString()
                rideLater.performClick()
            }


        }
    }

    private fun destroyUI() {
        if (map != null)
            map.onDestroy()
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
    }


    override fun onDestroy() {
        destroyUI()
        super.onDestroy()

    }

    companion object {
        private var home: Home? = null
        fun newInstance(): Home {
            if (home == null)
                home = Home()
            return home!!
        }
    }
}