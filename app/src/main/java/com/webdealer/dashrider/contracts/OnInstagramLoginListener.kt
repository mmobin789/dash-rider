package com.webdealer.dashrider.contracts

import com.webdealer.dashrider.api.Instagram

interface OnInstagramLoginListener {
    fun onSuccess(instagram: Instagram)
}