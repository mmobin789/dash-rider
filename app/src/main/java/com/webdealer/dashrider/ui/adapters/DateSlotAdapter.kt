package com.webdealer.dashrider.ui.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.contracts.OnListItemClickListener
import com.webdealer.dashrider.models.DateSlot
import kotlinx.android.synthetic.main.date_row.*

class DateSlotAdapter(private val list: List<DateSlot>, private val onListItemClickListener: OnListItemClickListener) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(onListItemClickListener, LayoutInflater.from(parent.context).inflate(R.layout.date_row, parent, false))

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dateSlot = list[position]
        holder.dowTV.text = dateSlot.dayOfWeek
        holder.dmTV.text = dateSlot.dayOfMonth.toString()
        holder.monthTV.text = dateSlot.month
        if (dateSlot.isSet) {
            holder.border.setBackgroundResource(R.drawable.btn_circle_bg)
            holder.dowTV.setTextColor(Color.WHITE)
            holder.dmTV.setTextColor(Color.WHITE)
        } else {
            holder.border.setBackgroundResource(R.drawable.circle_black_border_outline)
            holder.dowTV.setTextColor(Color.BLACK)
            holder.dmTV.setTextColor(Color.BLACK)
        }
    }
}