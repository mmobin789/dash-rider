package com.webdealer.dashrider.contracts

interface OnListItemClickListener {
    fun onListItemClicked(position: Int)
}