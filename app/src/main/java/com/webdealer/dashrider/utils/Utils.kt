package com.webdealer.dashrider.utils

import android.content.Context
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import android.util.Patterns
import id.zelory.compressor.Compressor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


object Utils {

    fun printHashKey(context: Context) {
        try {
            val info = context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i("HashKey", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun isValidEmail(email: String): Boolean {
        return email.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getCompressedFile(context: Context, file: File): File? {
        var compressed: File? = null
        Compressor(context).compressToFileAsFlowable(file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            compressed = it

                        }
                        , {
                    Log.d("Compression", it.toString())
                }
                )

        return compressed
    }

    fun getMaskedCardNumber(cardNumber: String): String {
        return if (cardNumber.isNotBlank()) {
            val last4digits = cardNumber.substring(cardNumber.length - 4, cardNumber.length)

            val x = StringBuilder()
            for (i in 0..cardNumber.length - 4) {
                x.append("*")
            }
            x.toString() + last4digits
        } else cardNumber
    }
}