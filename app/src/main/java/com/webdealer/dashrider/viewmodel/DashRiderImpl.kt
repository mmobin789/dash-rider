package com.webdealer.dashrider.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.webdealer.dashrider.api.Api.getWebApi
import com.webdealer.dashrider.api.Instagram
import com.webdealer.dashrider.contracts.OnApiListener
import com.webdealer.dashrider.contracts.OnInstagramLoginListener
import com.webdealer.dashrider.models.*
import com.webdealer.dashrider.ui.activities.Base
import com.webdealer.dashrider.ui.activities.Base.Companion.fcmData
import com.webdealer.dashrider.ui.fragments.BaseUI
import com.webdealer.dashrider.utils.AppStorage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DashRiderImpl : ViewModel() {
    private lateinit var disposable: Disposable

    private fun getBasicAuth(): Auth {
        val auth = Auth(AppStorage.getUser().email, "")
        auth.rideID = if (fcmData?.getString("rideID") != null) {
            val rideID = fcmData!!.getString("rideID")
            Log.i("rideID", rideID)
            rideID
        } else {
            "N/A"
        }

        return auth
    }

    fun getInstagramUser(base: Base, accessToken: String, onInstagramLoginListener: OnInstagramLoginListener) {
        disposable = getWebApi().getInstagramUser(accessToken).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        { instagram ->
                            val apiResponse = MutableLiveData<Instagram>()
                            apiResponse.postValue(instagram)
                            apiResponse.observe(base, Observer {

                                onInstagramLoginListener.onSuccess(it!!)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()

                })
    }

    fun login(base: Base, email: String, password: String, onApiListener: OnApiListener) {
        val data = Auth(email, password)
        disposable = getWebApi().login(data).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun register(base: Base, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().register(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }


    fun verifyPhone(base: BaseUI, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().phoneVerify(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun resendCode(base: BaseUI, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().resendCode(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun fcm(base: Base, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().fcm(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun addPaymentInfo(base: Base, paymentInfo: PaymentInfo, onApiListener: OnApiListener) {

        disposable = getWebApi().addPaymentInfo(paymentInfo).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }


    fun getCards(base: Base, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().getCardsList(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun deleteCard(base: Base, auth: Auth, onApiListener: OnApiListener) {

        disposable = getWebApi().deleteCard(auth).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun bookRide(base: Base, rideBooking: RideBooking, onApiListener: OnApiListener) {

        disposable = getWebApi().bookRide(rideBooking).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(it)
                            apiResponse.observe(base, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    onApiListener.onApiError(it.toString())
                })
    }

    fun realTimeDriverLocation(baseUI: BaseUI, onApiListener: OnApiListener) {
        val driverInfo = DriverInfo("", "")
        driverInfo.rideID = getBasicAuth().rideID
        disposable = getWebApi().realTimeTrackDriver(driverInfo).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(
                        { response ->
                            val apiResponse = MutableLiveData<ApiResponse>()
                            apiResponse.postValue(response)
                            apiResponse.observe(baseUI, Observer {

                                onApiListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()
                    onApiListener.onApiError(it.toString())
                })
    }
}