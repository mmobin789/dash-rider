package com.webdealer.dashrider.ui.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import com.webdealer.dashrider.R
import kotlinx.android.synthetic.main.activity_scheduled_ride_details.*
import kotlinx.android.synthetic.main.ride_row.*

class ScheduledRideDetails : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scheduled_ride_details)
        init()
    }

    private fun init() {
        setToolbarTitle("My Rides")
        line1.visibility = View.GONE
        trackDriver.setOnClickListener {
            startActivity(Intent(it.context, TrackRide::class.java))
        }
    }

    fun contactDriverDialog(v: View) {
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.contact_driver_dialog)
        dialog.show()
    }
}
