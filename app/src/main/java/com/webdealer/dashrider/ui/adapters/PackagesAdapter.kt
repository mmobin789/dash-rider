package com.webdealer.dashrider.ui.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webdealer.dashrider.R
import com.webdealer.dashrider.models.RidePackage
import kotlinx.android.synthetic.main.package_row.*

class PackagesAdapter(private val list: List<RidePackage>) : RecyclerView.Adapter<ViewHolder>() {
    private var lastCheckedPosition = 0

    init {
        list[0].isSelected = true
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(null, LayoutInflater.from(parent.context).inflate(R.layout.package_row, parent, false))
        vh.containerView.setOnClickListener {
            list[lastCheckedPosition].isSelected = false
            list[vh.adapterPosition].isSelected = true
            notifyItemChanged(vh.adapterPosition)
            notifyItemChanged(lastCheckedPosition)
            lastCheckedPosition = vh.adapterPosition

        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(vh: ViewHolder, position: Int) {
        val ridePackage = list[position]
        val color = if (ridePackage.isSelected) {

            vh.root.setBackgroundResource(R.drawable.color_primary_border)
            ContextCompat.getColor(vh.containerView.context, R.color.black)

        } else {
            vh.root.setBackgroundResource(R.drawable.gray_border)
            ContextCompat.getColor(vh.containerView.context, R.color.lgray)

        }
        vh.rb.isChecked = ridePackage.isSelected
        vh.rb.setTextColor(color)
        vh.validTV.setTextColor(color)
        vh.priceTV.setTextColor(color)
    }
}